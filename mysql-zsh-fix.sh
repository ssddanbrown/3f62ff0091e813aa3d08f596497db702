# BACKUP & SNAPSHOT BEFORE USE
# Files starting with hashes are just comments

# Gather required zeros to append
# Will create a "zeros" file in the current directory
# This has been calculated based upon 2007040 % 16384 = 8192 or <broken_file_size> % <default_page_size>
dd if=/dev/zero bs=1 count=8192 of=./zeros

# Append zeroes to invalid file
cat zeros >> /var/lib/mysql/#innodb_redo/#ib_redo6

# Restart MySQL
systemctl restart mysql.service